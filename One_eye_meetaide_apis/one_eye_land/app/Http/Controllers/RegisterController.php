<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use App\Models\User;

class RegisterController extends Controller
{
    public function user()
    {
        return User::orderBy('name')->get();
    }
    public function register(Request $request)
    {
        return User::create([
            'name'=>$request->input(key:'name'),
            'email'=>$request->input(key:'email'),
            'password'=>Hash::make($request->input(key:'password')),
        ]);
    }

    public function login(Request $request)
    {
       if(!Auth::attempt([
           'email'=>$request->input(key:'email'),
           'password'=>$request->input(key:'password')
       ]))
       {
           return response([
               'message'=>'Invalid credentials'
           ]);
       }

       $user=Auth::user();
       return $user;
    }
}
