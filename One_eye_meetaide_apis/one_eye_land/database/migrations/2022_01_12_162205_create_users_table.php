<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->id();
            $table->string(column:'name');
            $table->string(column:'email')->unique;
            $table->string(column:'password');
            $table->timestamps();
        });

        // Schema::create('users', function (Blueprint $table) {
        //     $table->id();
        //     $table->string(column:'_class');
        //     $table->string(column:'email');
        //     $table->string(column:'emailFooter');
        //     $table->string(column:'emailLogo');
        //     $table->string(column:'frg');
        //     $table->boolean(column:'isActive');
        //     $table->boolean(column:'isEmailCustomized');
        //     $table->boolean(column:'isEmailVerified');
        //     $table->document(column:'meetingLink');
        //     $table->string(column:'name');
        //     $table->string(column:'password');
        //     $table->array(column:'roles');
        //     $table->string(column:'stripeId');
        //     $table->document(column:'subscription');
        //     $table->timestamps();
        // });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
