<?php

use Illuminate\Support\Facades\Route;

use App\Http\Controllers\RegisterController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//for User Account
Route::get('/', function()
{
    return "Welcome Page";
});
// Route::post('/register',function()
// {
//       return User::create([
//         'name'=>'Product one',
//         'slug'=>'product-one',
//         'description'=>'This is Product one',
//         'price'=>'99.99',
//     ]);
// });

Route::get('/register',[RegisterController::class,'index']);
Route::post('/register',[RegisterController::class,'store']);