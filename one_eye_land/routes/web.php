<?php

use Illuminate\Support\Facades\Route;
use App\Models\Feed;
use App\Models\User;

use App\Http\Controllers\FeedController;



/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//for User Account
Route::get('/', function()
{
    return "Welcome Page";
});
Route::get('/listuser',function()
{
    return Feed::all();
});
//Route::post('/storeuser', [FeedController::class,'storeuser']);
Route::post('/storeuser',function()
{
    return Feed::create(
        [
            'name'=>'Ponmari',
            'slug'=>'ponmari',
            'description'=>'this is useful',
            'price'=>'99.99'
        ]
        );
});